//
//  TestClass.m
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "TestClass.h"

@implementation TestClass

- (void) dealloc{
    NSLog(@"Test class dealloced %p", self);
    [super dealloc];
}
@end
