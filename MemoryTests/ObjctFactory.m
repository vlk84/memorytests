//
//  ObjctFactory.m
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/27/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ObjctFactory.h"
#import "TestClass.h"

@implementation ObjctFactory

- (TestClass*)newTestObject{
    return [[[TestClass alloc] init] autorelease];
}

- (TestClass*)testObject{
    return [[[TestClass alloc] init] autorelease];
}

@end
