//
//  ARCViewController.m
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/27/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ARCViewController.h"
#import "TestClass.h"
#import "ObjctFactory.h"

@interface ARCViewController ()

@end

@implementation ARCViewController{
    TestClass *obj1;
    TestClass *obj2;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
	// Do any additional setup after loading the view.
    ObjctFactory *factory = [[ObjctFactory alloc] init];
    obj1 = [factory testObject];
    obj2 = [factory newTestObject];
    NSLog(@"%@: Created 2 objects: %p and %p",NSStringFromClass([self class]), obj1, obj2);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:tap];
}

- (void) tapAction:(id)sender{
    NSLog(@"%@: Now I have 2 objects: %@ and %@",NSStringFromClass([self class]), obj1, obj2);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
