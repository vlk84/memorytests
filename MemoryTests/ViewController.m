//
//  ViewController.m
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ViewController.h"
#import "TestClass.h"
#import "ARCObjectsFactory.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    ARCObjectsFactory *factory = [[ARCObjectsFactory alloc] init];
    TestClass *obj1 = [factory newTestObject];
    TestClass *obj2 = [factory testObject];
    
    NSLog(@"%@: Created 2 objects: %p and %p",NSStringFromClass([self class]), obj1, obj2);
    [factory release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
