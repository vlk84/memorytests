//
//  ObjectsFactory.m
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ARCObjectsFactory.h"
#import "TestClass.h"

@implementation ARCObjectsFactory

- (TestClass*)newTestObject{
    return [[TestClass alloc] init];
}

- (TestClass*)testObject{
    return [[TestClass alloc] init];
}
@end
