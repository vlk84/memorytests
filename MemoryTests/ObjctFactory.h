//
//  ObjctFactory.h
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/27/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TestClass;


@interface ObjctFactory : NSObject
- (TestClass*)newTestObject;
- (TestClass*)testObject;
@end
