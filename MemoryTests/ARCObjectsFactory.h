//
//  ObjectsFactory.h
//  MemoryTests
//
//  Created by Vladimir Kolbun on 2/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TestClass;



@interface ARCObjectsFactory : NSObject

- (TestClass*)newTestObject;
- (TestClass*)testObject;

@end
